from django.contrib import admin
from django.urls import include, path
from cocktailapp.views import list_cocktails, create_cocktail, cocktail_details
from django.views.generic.base import RedirectView
from django.contrib.auth import views as auth_views


urlpatterns = [

    path('admin/', admin.site.urls),
    path('create/', create_cocktail, name="create_cocktail"),
    path('cocktails/', list_cocktails, name="list_cocktails"),
    path('details/', cocktail_details, name="cocktail_details"),
]
