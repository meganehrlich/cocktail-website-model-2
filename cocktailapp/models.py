from django.db import models

# Create your models here.
class Cocktail(models.Model):
    name = models.CharField(max_length=120)
    creator = models.CharField(max_length=120, null=True, blank=True)
    image = models.URLField(null=True, blank=True)
    liquor = models.ManyToManyField("Liquor", related_name="cocktail", blank=True)
    description = models.TextField(null=True, blank=True)
    on_ice = models.BooleanField(null=True, blank=True)

    def __str__(self):
        return self.name

class Liquor(models.Model):
    name = models.CharField(max_length=100)
    def __str__(self):
        return self.name